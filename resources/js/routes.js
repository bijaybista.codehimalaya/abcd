import UserComponent from './components/UserComponent.vue';
import UserComponent from './components/CreateComponent.vue';
import UserComponent from './components/EditComponent.vue';


export const routes = [
  {
      name: 'users',
      path: '/',
      component: UserComponent
  },
  {
      name: 'create',
      path: '/create',
      component: CreateComponent
  },
  {
    name: 'edit',
    path: '/edit/:id',
    component: EditComponent
}

];